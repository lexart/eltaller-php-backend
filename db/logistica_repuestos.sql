-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 23-02-2019 a las 00:23:04
-- Versión del servidor: 5.5.8
-- Versión de PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Base de datos: `logistica_repuestos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE IF NOT EXISTS `productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `codigo` varchar(200) NOT NULL,
  `medida` varchar(50) NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `calidad` varchar(50) NOT NULL,
  `expiracion` datetime NOT NULL,
  `fechaCreado` datetime NOT NULL,
  `fechaEditado` datetime NOT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcar la base de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `nombre`, `codigo`, `medida`, `tipo`, `calidad`, `expiracion`, `fechaCreado`, `fechaEditado`, `activo`) VALUES
(1, 'CAFE MOCA 5kg GRANO', 'ABC123', 'KG', 'GRANO', 'ALTA', '2019-02-19 19:48:20', '2019-02-22 19:48:35', '2019-02-22 19:48:37', 1),
(2, 'CAFE FAMILIAR 5kg GRANO', 'ABC111', 'KG', 'GRANO', 'ALTA', '2032-02-22 19:49:13', '2019-02-22 19:49:22', '2019-02-22 19:49:24', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `usuario` varchar(200) NOT NULL,
  `clave` varchar(500) NOT NULL,
  `tipo` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Volcar la base de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `usuario`, `clave`, `tipo`) VALUES
(1, 'Alex Casadevall', 'lexcasa', 'b75bd008d5fecb1f50cf026532e8ae67', 'admin'),
(2, 'Jose Pedro 3', 'pp123', 'b75bd008d5fecb1f50cf026532e8ae67', 'admin'),
(3, 'Juan', 'jman', '202cb962ac59075b964b07152d234b70', 'admin'),
(4, 'Manuel', 'manu_1', '9450476b384b32d8ad8b758e76c98a69', 'user'),
(5, 'Juan pablo', 'jp_chivito', '7a7e28631835bd778c21885df4822ca0', 'user'),
(6, 'Pedro', 'pr_123', '167f349b3b8aae664624d27de95fb9e8', 'user'),
(7, '', '', 'd41d8cd98f00b204e9800998ecf8427e', 'admin'),
(8, '', '', 'd41d8cd98f00b204e9800998ecf8427e', 'admin'),
(9, '', '', 'd41d8cd98f00b204e9800998ecf8427e', 'admin'),
(10, '', '', 'd41d8cd98f00b204e9800998ecf8427e', 'admin'),
(11, '', '', 'd41d8cd98f00b204e9800998ecf8427e', 'admin'),
(12, 'Jose', 'pepe', 'd41d8cd98f00b204e9800998ecf8427e', 'admin'),
(13, 'Jose', 'pepe', 'd41d8cd98f00b204e9800998ecf8427e', 'user'),
(14, 'Jose', 'pepe', 'd41d8cd98f00b204e9800998ecf8427e', 'user');
