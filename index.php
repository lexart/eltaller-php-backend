<?php 
	// RUTEO
	$ruteo = array(
				"model"  => "",
				"action" => "",
				"params" => ""
			);

	$result = array("error" => "api not found");

	if(isset($_GET['model']) && isset($_GET['action'])){
		
		$ruteo["model"] 	= $_GET['model'];
		$ruteo["action"] 	= $_GET['action'];
		
		if($ruteo["action"] == "all"){

			if($ruteo["model"] == "productos"){

				// LISTADO DE PRODUCTOS
				require('components/productos/index.php');

			} else if($ruteo["model"] == "clientes"){

				// LISTADO DE PRODUCTOS
				require('components/clientes/index.php');

			} else if($ruteo["model"] == "proveedores"){

				// LISTADO DE PRODUCTOS
				require('components/proveedores/index.php');

			} else if($ruteo["model"] == "autos"){

				// LISTADO DE PRODUCTOS
				require('components/autos/index.php');

			}
			 else {
				$result = array("error" => $ruteo["model"] . " not found");
			}
			
		} else if($ruteo["action"] == "single"){

			if($ruteo["model"] == "productos"){
				// LISTADO DE PRODUCTOS
				require('components/productos/get.php');

			} else if($ruteo["model"] == "clientes"){
				// LISTADO DE PRODUCTOS
				require('components/clientes/get.php');

			} else if($ruteo["model"] == "proveedores"){
				// LISTADO DE PRODUCTOS
				require('components/proveedores/get.php');

			} else if($ruteo["model"] == "usuarios" || $ruteo["model"] == "usuario-by-id"){
				// LISTADO DE PRODUCTOS
				require('components/usuarios/get.php');

			} else {
				$result = array("error" => $ruteo["model"] . " not found");
			}
			
		} else if($ruteo["action"] == "insert"){
			if($ruteo["model"] == "autos"){

				// LISTADO DE PRODUCTOS
				require('components/autos/post.php');

			} else if($ruteo["model"] == "login" || $ruteo["model"] == "usuario-new" || $ruteo["model"] == "usuario-edit"){
				// LISTADO DE PRODUCTOS
				require('components/usuarios/post.php');
			}
		} else if($ruteo["action"] == "interaction"){
			if($ruteo["model"] == "producto-editar"){
				require('components/productos/post.php');
			}
		}
		else {
			$result = array("error" => true);
		}
	}
	

	header("Content-Type: application/json");
	echo json_encode( $result );
?>