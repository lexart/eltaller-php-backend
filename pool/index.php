<html>
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="css/style.css">

    <title>UPLOAD FILES</title>
</head>
<body>
	<form class="container" method="POST" action="upload.php" enctype="multipart/form-data">
		<h4>Subir archivo</h4>
		<input type="text" name="nombre" placeholder="Nombre archivo" class="form-control">
		<br>
		<input type="file" name="archivo">
		<br>
		<button type="submit" class="btn btn-success">Subir archivo</button>
	</form>
</body>
</html>