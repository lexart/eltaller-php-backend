var btn 		= $('#interaccion--uno');
var listaCont   = $('.lista--productos');

var WEB_GET 	= "controllers/get/";
var WEB_POST 	= "controllers/post/";

var productos = [];

// GET
$.get(WEB_GET + "productos.js.controller.php").then( function  (data) {
	productos = data;
})

// EVENTO
btn.click( function (e){
	var itemsLista = listaCont.find('li');

	if(itemsLista.length < productos.length){
		productos.map( function (item, i){
			console.log("Producto: ", item.nombre);
			listaCont.append("<li>" + item.nombre + "</li>");
		});
	}
});


var eliminarProducto = function (){
	var btnEliminar = $('.eliminar');
	var interaction = {
		id: 0,
		activo: 0
	};

	btnEliminar.click( function (event){
		var el = $(this);
		var id = el.attr('id').split('-')[1];
		interaction.id = id;

		console.log("eliminar :", interaction);

		$.post(WEB_POST + "producto.js.controller.php", interaction).then( function (data){
			console.log("post data: ", data);
		})
		return false;
	});
};
eliminarProducto();

var verProductoPorCodigo 	= function (){
	var modal 	= $('#exampleModal');
	var wait 	= modal.find('.wait');
	var detail  = modal.find('.product_detail');

	var btnVer 	= $('.ver_producto');

	btnVer.click( function (){
		// REMOVE LI
		detail.hide();
		detail.find('li').remove();

		var el 		= $(this);
		var codigo 	= el.attr('id').split('-')[1];

		var titulo 	= modal.find('.modal-title');

		titulo.text("Producto COD: " + codigo);
		wait.text("Cargando productos ...");
		
		wait.fadeIn("slow");

		$.get(WEB_GET + "productos.js.controller.php?codigo=" + codigo).then( function  (data) {
			
			var producto = data[0];
			console.log("producto :: ", producto);

			detail.append("<li> Nombre: "+producto.nombre+"</li>");
			detail.append("<li> Tipo: "+producto.tipo+"</li>");
			detail.append("<li> Medida: "+producto.medida+"</li>");
			detail.append("<li> Calidad: "+producto.calidad+"</li>");
			var fechaExp  	= new Date(producto.expiracion);
			var hoy 		= new Date();
			var vencido 	= hoy > fechaExp;
			var nwFechaExp 	= fechaExp.getDate() + "/" + ( fechaExp.getMonth() + 1) + "/" + fechaExp.getFullYear();
			detail.append("<li> Fecha Expiración: "+nwFechaExp+"</li>");
			detail.append("<li> Vencido: <b>"+(vencido == true ? 'SI' : 'NO')+"</b></li>");

			wait.fadeOut( function (){
				detail.fadeIn();
			});
		})



		console.log("codigo: ", codigo);
	});

};
verProductoPorCodigo();

var editarProductoPorCodigo = function (){
	var btnEditar 	= $('.editar_producto');
	var btnGuardar	= $('#guardarProducto');
	var waitSuccess = $('.wait--success');
	var codigoGbl;

	btnEditar.click( function (){
		waitSuccess.hide();
		var form   = $('#editarProductoModal').find('form');
		var codigo = $(this).attr('id').split('-')[1];
		codigoGbl  = codigo;

		console.log("form: ", form);
		$.get(WEB_GET + "productos.js.controller.php?codigo=" + codigo).then( function  (data) {
			var producto = data[0];
			
			form.find('#nombre').val( producto.nombre );
			form.find('#tipo').val( producto.tipo );
			form.find('#calidad').val( producto.calidad );
			form.find('#expiracion').val( producto.expiracion.split(" ")[0] );
			form.find('#activo').prop("checked", producto.activo == "1" ? true : false);
			console.log("producto: ", producto);
		});
	});


	btnGuardar.click( function  () {
		var form   = $('#editarProductoModal').find('form');

		var producto = {
			codigo: "",
			tipo: "",
			calidad: "",
			expiracion: "",
			activo: ""
		};
		producto.codigo 	= codigoGbl;
		producto.tipo 		= form.find('#tipo').val();
		producto.calidad 	= form.find('#calidad').val();
		producto.expiracion = form.find('#expiracion').val();
		producto.activo 	= form.find('#activo').prop('checked') ? 1 : 0;
		
		console.log("save producto: ", producto);
		
		$.post(WEB_POST + "producto.js.controller.php", producto).then( function (data){
			console.log("post data: ", data);
			if(data.success){
				waitSuccess.text(data.success);
				waitSuccess.fadeIn();

				setTimeout( function (){
					window.location.reload();
				}, 2000);
			}
		})
	})


};
editarProductoPorCodigo();

var subirImagenProducto 	= function (){
	var btnSubir 	= $('.subir_imagen');
	btnSubir.click( function () {
		var codigo 		= $(this).attr('id').split('-')[1];
		var form   		= $('#subirImagenProductoModal').find('form');
		var fileInput   = form.find('#imagen');
		var imgResult;
		var imgCont 	= form.find('#imgResultante');

		fileInput.change( function (ev) {
			console.log("event: ", ev, $(this)[0].files);
			var img = $(this)[0].files[0];

			var fr = new FileReader();
			

			fr.addEventListener('load', function (e){
				imgResult = e.target.result;
				console.log("target: ", e.target.result);
				imgCont.attr('src', imgResult);
				imgCont.fadeIn("slow");
			})

			fr.readAsDataURL( img );
		
		})
	})
};
subirImagenProducto();