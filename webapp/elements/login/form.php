<?php 
	$error = "";
	if(isset($_GET) && !empty($_GET["error"])){
		$error = $_GET["error"];
	}
?>
<div class="container">
	<form id="login" method="POST" action="controllers/post/login.controller.php">
		<h3 class="text-center">INGRESAR AL SISTEMA</h3>
		<input type="text" name="usuario" class="form-control inputs-formulario">
		<br>
		<input type="password" name="clave" class="form-control inputs-formulario">
		<br>
		<?php if($error){ ?>
			<div class="alert alert-danger" role="alert">
			  <?php echo $error; ?>
			</div>
		<?php } ?>
		<button class="btn btn-primary boton" type="submit">Login</button>
	</form>
</div>