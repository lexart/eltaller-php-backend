<div class="container">
	<h2>Usuarios</h2>
	<?php 
		$action = "";
		if(isset($_GET) && !empty($_GET["action"])){
			$action = $_GET["action"];
		}
	?>

	<?php if($action == "all") { ?>
	<div class="">
		<a href="usuarios.php?action=new" class="btn btn-primary float-right">Crear usuario</a>
	</div>
	<table class="table table--software">
		<thead>
			<tr>
				<th>ID</th>
				<th>Nombre</th>
				<th>Usuario</th>
				<th>Tipo</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php for ($i=0; $i < count($usuarios) ; $i++) { 
				$usr = $usuarios[$i];
			?>
				<tr>	
					<td><?php echo $usr->id; ?></td>
					<td><?php echo $usr->nombre; ?></td>
					<td><?php echo $usr->usuario; ?></td>
					<td><?php echo $usr->tipo; ?></td>
					<td class="text-right">
						<a class="btn btn-outline-primary" href="usuarios.php?action=edit&id=<?php echo $usr->id; ?>">
							Editar usuario
						</a>
					</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
	<?php } ?>

	<?php if($action == "new" || $action == "edit") { ?>
		<?php 
			$id = "";
			if(isset($_GET) && !empty($_GET["id"])){
				$id 	= $_GET["id"];
				$usr 	= $result;
			}
		?>
		<form id="login" method="POST" action="controllers/post/usuario.controller.php">
			<?php if(!empty($id) && $action == "edit") {?>
				<input name="id" type="number" readonly="" value="<?php echo $usr->id ? $usr->id : '' ?>" class="form-control">
				<br>
			<?php } ?>
			<input required type="text" name="nombre" class="form-control" placeholder="Nombre" value="<?php echo $usr->nombre ? $usr->nombre : '' ?>">
			<br>
			<input required type="text" value="<?php echo $usr->usuario ? $usr->usuario : '' ?>" name="usuario" class="form-control" placeholder="Usuario">
			<br>
			<select name="tipo" class="form-control">
				<option value="admin" <?php echo ($usr->tipo == "admin") ? 'selected' : '' ; ?> >ADMIN</option>
				<option value="user" <?php echo ($usr->tipo == "user") ? 'selected' : '' ; ?> >USER</option>
			</select>
			<br>
			<input type="password" name="clave" class="form-control" placeholder="Clave">
			<br>
			<?php 
			$success = "";
			if(isset($_GET) && !empty($_GET["success"])){
				$success = $_GET["success"];
			}
			if($success){ ?>
				<div class="alert alert-success" role="alert">
				  <?php echo $success; ?>
				</div>
			<?php } ?>

			<?php 
			$error = "";
			if(isset($_GET) && !empty($_GET["error"])){
				$error = $_GET["error"];
			}
			if($error){ ?>
				<div class="alert alert-danger" role="alert">
				  <?php echo $error; ?>
				</div>
			<?php } ?>
			<button class="btn btn-default" type="reset">Cancelar</button>
			<button class="btn btn-success" type="submit"><?php echo !empty($id) ? 'Editar' : 'Guardar'; ?></button>
		</form>
	<?php } ?>
</div>