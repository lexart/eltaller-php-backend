<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="dashboard.php">Sistema Logistica</a>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="usuarios.php?action=all">Usuarios</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="productos.php?action=all">Productos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Clientes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Stock</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Proveedores</a>
      </li>
    </ul>
  </div>
</nav>