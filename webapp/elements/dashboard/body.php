<div class="container-fluid">
	<h1>Dashboard</h1>
</div>
<div class="container-fluid text-right">
	<button class="btn btn-primary">Crear tarea</button>
</div>
<?php 
	$tareas = array(
					"toDo" => array(
								array(
									"titulo" 		=> "Hacer API - Obtener proyectos",
									"descripcion" 	=> "Some quick example text to build on the card title and make up the",
									"id" 			=> 101
								),
								array(
									"titulo" 		=> "Hacer API - Obtener proyecto por ID",
									"descripcion" 	=> "Some quick example text to build on the card title and make up the",
									"id" 			=> 102
								),
								array(
									"titulo" 		=> "ASAP - Obtener proyectos",
									"descripcion" 	=> "Some quick example text to build on the card title and make up the",
									"id" 			=> 101
								),
								array(
									"titulo" 		=> "ASAP - Obtener proyecto por ID",
									"descripcion" 	=> "Some quick.",
									"id" 			=> 102
								)
							),
					"inProgress" => array(
								array(
									"titulo" 		=> "Revisar base de datos",
									"descripcion" 	=> "Some quick example text to build on the card title and make up the",
									"id" 			=> 103
								),
								array(
									"titulo" 		=> "Deploy a ambiente test",
									"descripcion" 	=> "Some quick example text to build on the card title and make up the",
									"id" 			=> 104
								),
								array(
									"titulo" 		=> "Revisar base de datos",
									"descripcion" 	=> "Some quick example text to build on the card title and make up the",
									"id" 			=> 1233
								),
								array(
									"titulo" 		=> "Deploy a ambiente test",
									"descripcion" 	=> "Some quick example text to build on the card title and make up the",
									"id" 			=> 12322
								)
							)
				);

?>
<div class="container-fluid board--component">
	<div class="row">
		<div class="col-3">
			<h3>TO-DO</h3>
			<div class="board--component__container">
				<?php for ($i=0; $i < count($tareas["toDo"]) ; $i++) { 
					$tarea 	= $tareas["toDo"][$i];
				?>
					<div class="board--component__cards">
						<div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
						  <div class="card-header"><?php echo $tarea["titulo"]; ?></div>
						  <div class="card-body">
						    <p class="card-text">
						    	<?php echo $tarea["descripcion"]; ?>
						    </p>
						    <a target="_new" href="tareas.php?action=edit&id=<?php echo $tarea["id"]; ?>" class="btn btn-secondary">
						    	VER TAREA
						    </a>
						  </div>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
		<div class="col-3">
			<h3>IN-PROGRESS</h3>
			<div class="board--component__container">
				<?php for ($i=0; $i < count($tareas["inProgress"]) ; $i++) { 
					$tarea 	= $tareas["inProgress"][$i];
				?>
					<div class="board--component__cards">
						<div class="card text-white bg-warning mb-3" style="max-width: 18rem;">
						  <div class="card-header"><?php echo $tarea["titulo"]; ?></div>
						  <div class="card-body">
						    <p class="card-text">
						    	<?php echo $tarea["descripcion"]; ?>
						    </p>
						    <a href="tareas.php?action=edit&id=<?php echo $tarea["id"]; ?>" class="btn btn-secondary">
						    	VER TAREA
						    </a>
						  </div>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
		<div class="col-3">
			<h3>IN-REVIEW</h3>
			<div class="board--component__container"></div>
		</div>
		<div class="col-3">
			<h3>DONE</h3>
			<div class="board--component__container"></div>
		</div>
	</div>
</div>