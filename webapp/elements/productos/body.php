<div class="container">
	<h2>Productos</h2>
	<ul class="lista--productos">
	</ul>

	<div class="row">
		<div class="col-8"></div>
		<div class="col-4">
			<form method="GET" action="productos.php?action=all">
				<div class="input-group">
				  <input type="hidden" name="action" value="<?php echo $_GET['action'] ?>">
				  <input type="text" class="form-control" placeholder="Buscar por código" value="<?php echo $_GET['codigo'] ?>" name="codigo" required>
				  <div class="input-group-append" id="button-addon4">
				    <button class="btn btn-outline-secondary" type="submit">Buscar</button>
				  </div>
				</div>
			</form>
		</div>
	</div>
	<table class="table table--software">
		<thead>
			<tr>
				<th>ID</th>
				<th>Nombre</th>
				<th>COD</th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php 
				$hoy = strtotime( date("Y-m-d") );
				for ($i=0; $i < count($productos) ; $i++) { 
					$bg  		= "";
					$producto 	= $productos[$i];
					$expiracion = strtotime($producto->expiracion);

					if($hoy > $expiracion){
						$bg  = "bg-warning";
					}
				?>
				<tr class="<?php echo $bg; ?>">
					<td><?php echo $producto->id; ?></td>
					<td><?php echo $producto->nombre; ?></td>
					<td><?php echo $producto->codigo; ?></td>
					<td>
						<a href="#" class="btn btn-outline-primary subir_imagen" id="imgProd-<?php echo $producto->codigo; ?>" data-toggle="modal" data-target="#subirImagenProductoModal">
							Subir imágen
						</a>
					</td>
					<td>
						<a href="#" class="btn btn-outline-primary editar_producto" id="editProd-<?php echo $producto->codigo; ?>" data-toggle="modal" data-target="#editarProductoModal">
							Editar
						</a>
					</td>
					<td>
						<a href="#" id="prod-<?php echo $producto->id; ?>" class="btn btn-outline-danger eliminar">
							Eliminar
						</a>
					</td>
					<td>
						<a href="#" id="prod-<?php echo $producto->codigo; ?>" class="btn btn-outline-success ver_producto" data-toggle="modal" data-target="#exampleModal">
							Ver
						</a>
					</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>

	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <div class="alert alert-info wait" role="alert" style="display: none"></div>
	        <ul class="product_detail" style="display: none;"></ul>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- EDITAR PRODUCTO -->
	<div class="modal fade" id="editarProductoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Editar producto</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<div class="alert alert-success wait--success" role="alert" style="display: none"></div>
	        <form>
	        	<input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre" readonly>
	        	<br>
	        	<input type="text" name="tipo" id="tipo" class="form-control" placeholder="Tipo">
	        	<br>
	        	<input type="text" name="calidad" id="calidad" class="form-control" placeholder="Calidad">
	        	<br>
	        	<input type="date" name="expiracion" id="expiracion" class="form-control" placeholder="Expiración">
	        	<br>
	        	<div class="checkbox--container">
		        	<label>Activo</label>
		        	<div class="custom-control custom-checkbox">
					  <input type="checkbox" class="custom-control-input" id="activo" name="activo">
					   <label class="custom-control-label" for="activo"></label>
					</div>
				</div>
	        	<br>
	        </form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
	        <button type="button" class="btn btn-success" id="guardarProducto">Guardar</button>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- SUBIR IMAGEN -->
	<div class="modal fade" id="subirImagenProductoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Subir imagen</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<div class="alert alert-success wait--success" role="alert" style="display: none"></div>
	        <form>
	        	<input type="text" name="nombre" id="nombreImagen" class="form-control">
	        	<br>
	        	<input type="file" name="imagen" id="imagen">
	        	<br>
	        	<img src="" id="imgResultante" style="width: 150px; display: none">
	        </form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
	        <button type="button" class="btn btn-success" id="guardarImagen">Subir</button>
	      </div>
	    </div>
	  </div>
	</div>
</div>