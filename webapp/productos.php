<?php 
	require('config/conn.php');
	session_start();

	if(isset($_SESSION) && !empty($_SESSION["usuario"])){
		$usuario = $_SESSION["usuario"];
	} else {
		header("location: ".BASEURL);
	}

	// CALL CONTROLLER
	require('controllers/get/productos.controller.php');

	// HEAD
	include('elements/head.php');
	
		// MENU 
		include('elements/menu.php');

		// VIEW
		include('elements/productos/body.php');

	// FOOTER
	include('elements/footer.php');
?>