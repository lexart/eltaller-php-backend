<?php 
	require('../../config/conn.php');

	$conn 		= new Conn();
	$productos 	= array();

	$url 	= "?model=productos&action=all";

	// OBTENER USUARIO
	$id = "";
	if(isset($_GET) && !empty($_GET["codigo"])){
		$codigo 	= $_GET["codigo"];
		$url  	= "?model=productos&action=single&codigo=".$codigo;
		$result = $conn->_getQuery($url);

		if(!$result->error){
			$productos = $result;
		}
	} else {
		$result = $conn->_getQuery($url);

		if(!$result->error){
			$productos = $result;
		}
	}

	header("Content-Type: application/json");
	echo json_encode( $productos );
?>