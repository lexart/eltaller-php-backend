<?php 
	session_start();
	require('../../config/conn.php');
	$conn 	= new Conn();

	// URL LOGIN  ?model=login&action=insert
	$url 	= "?model=login&action=insert";
	$acceso = $_POST;

	$result = $conn->_postQuery($acceso,$url);

	if(!$result->error){
		$_SESSION["usuario"] = $result;
		header("location: ".BASEURL."dashboard.php");
	} else {
		header("location: ".BASEURL."?error=" . $result->error);
	}
?>