<?php 

	//
	require('config/conn.php');

	// CLASSES
	require('classes/Proveedor.php');

	$conn 			= new Conn();
	$objProveedor 	= new Proveedor();

	$rut 		= "";
	if(isset($_GET['rut'])){
		$rut = $_GET['rut'];
	}
	$result = $objProveedor->obtenerProveedorPorRut($conn,$rut);

?>