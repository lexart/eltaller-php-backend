<?php 
	
	// DEEP LINK
	// php://input
	$post = json_decode(file_get_contents("php://input"), true);

	require('config/conn.php');

	// CLASSES
	require('classes/Auto.php');

	$conn 			= new Conn();
	$objAuto		= new Auto();

	$result 		= $objAuto->insertarAuto($conn, $post);
?>