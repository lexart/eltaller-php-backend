<?php 
	
	// DEEP LINK
	// php://input
	$post = json_decode(file_get_contents("php://input"), true);

	require('config/conn.php');

	// CLASSES
	require('classes/Producto.php');

	$conn 			= new Conn();
	$objProducto    = new Producto();

	$result 		= $objProducto->inter_actualizarProducto($conn, $post);
?>