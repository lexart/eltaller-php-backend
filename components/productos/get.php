<?php 

	//
	require('config/conn.php');

	// CLASSES
	require('classes/Producto.php');

	$conn 			= new Conn();
	$objProducto 	= new Producto();

	$codigo 		= "";
	if(isset($_GET['codigo'])){
		$codigo = $_GET['codigo'];
	}
	$result = $objProducto->obtenerProductosPorCodigo($conn,$codigo);

?>