<?php 

	//
	require('config/conn.php');

	// CLASSES
	require('classes/Cliente.php');

	$conn 			= new Conn();
	$objCliente 	= new Cliente();

	$documento 		= "";
	if(isset($_GET['documento'])){
		$documento = $_GET['documento'];
	}
	$result = $objCliente->obtenerClientePorDocumento($conn,$documento);

?>