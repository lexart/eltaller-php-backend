<?php 


class Cliente {

	public function obtenerClientes($conn){
		$result = $conn->query("clientes");
		return json_decode( $result );
	}

	public function obtenerClientePorDocumento($conn,$documento){
		$clientes 		 = json_decode( $conn->query("clientes") );
		$cliEncontrado  = "";

		for ($i=0; $i < count($clientes); $i++) { 
			if($clientes[$i]->documento == $documento){
				$cliEncontrado = $clientes[$i];
			}
		}
		if($cliEncontrado){
			return $cliEncontrado;
		} else {
			return array("error" => "cliente no encontrado para el DOCUMENTO: " . $documento);
		}
		
	}
}

?>