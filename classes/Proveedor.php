<?php 

class Proveedor {

	public function obtenerProveedores($conn){
		$result = $conn->query("proveedores");
		return json_decode( $result );
	}

	public function obtenerProveedorPorRut($conn,$rut){
		$proveedores    = json_decode( $conn->query("proveedores") );
		$provEncontrado  = "";

		for ($i=0; $i < count($proveedores); $i++) { 
			if($proveedores[$i]->rut == $rut){
				$provEncontrado = $proveedores[$i];
			}
		}
		if($provEncontrado){
			return $provEncontrado;
		} else {
			return array("error" => "proveedor no encontrado para el RUT: " . $rut);
		}
		
	}
}

?>