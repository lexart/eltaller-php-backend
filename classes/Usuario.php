<?php
/**
* 
*/
class Usuario {
	
	public function login($conn,$usuario){
		// ENCRIPTAR CLAVE
		$usuario["clave"] = md5($usuario["clave"]);

		$sql = "SELECT * FROM usuarios
				WHERE usuario = '$usuario[usuario]'
				AND clave 	  = '$usuario[clave]'
		";
		$result = $conn->query($sql);

		if($result){
			return $result[0];
		} else {
			return array("error" => "usuario y/o clave incorrecta.");
		}
		
	}

	public function obtenerUsuarios($conn,$id) {

		// VERIFICAR SI ES ADMIN
		$sql = "SELECT * FROM usuarios
			WHERE id = '$id' AND tipo = 'admin'
		";
		$result = $conn->query($sql);

		if($result){
			// OBTENER USUARIOS
			$sql 	= "SELECT id, nombre, usuario, tipo FROM usuarios WHERE tipo != 'admin'";
			$result = $conn->query($sql);
			
			if($result){
				return $result;
			} else {
				return array("error" => "no existe usuarios");
			}
			
		} else {
			return array("error" => "usuario no tiene permisos");
		}
	}

	public function ingresarUsuario($conn, $usuario){
		$usuario["clave"] = md5($usuario["clave"]);
		
		$sql 	= "INSERT INTO usuarios
					(nombre, usuario, clave, tipo)
				   VALUES
				   	('$usuario[nombre]','$usuario[usuario]','$usuario[clave]','$usuario[tipo]')
		";
		$result = $conn->query($sql);

		if(empty($result)){
			return array("success" => "Usuario creado correctamente.");
		} else {
			return array("error" => true, "sql" => $sql);
		}
	}

	public function obtenerUsuarioPorId($conn,$id){
		$sql = "SELECT id, nombre, usuario, tipo FROM usuarios WHERE id='$id' AND tipo <> 'admin'";

		$result = $conn->query($sql);

		if($result){
			return $result[0];
		} else {
			return array("error" => "Usuario con ID: ". $id . " no encontrado.");
		}
	}

	public function editarUsuario($conn, $usuario){
		$sql = "UPDATE usuarios SET 
			nombre 	= '$usuario[nombre]',
			usuario = '$usuario[usuario]',
			tipo 	= '$usuario[tipo]'
		";

		if(empty($usuario["clave"])){
			$sql .= "
				WHERE id = '$usuario[id]'
			";
		} else {
			$usuario["clave"] = md5($usuario["clave"]);
			$sql .= "
				,
				clave = '$usuario[clave]'
				WHERE id = '$usuario[id]'
			";
		}

		$result = $conn->query($sql);
		
		if(empty($result)){
			return array("success" => "Usuario editado correctamente.");
		} else {
			return array("error" => true, "sql" => $sql);
		}
	}

}

?>