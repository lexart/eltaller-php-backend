<?php 


class Producto {

	public function obtenerProductos($conn){
		$sql 	= "SELECT * FROM productos";
		$result = $conn->query($sql);

		if(!empty($result)){
			return $result;
		} else {
			return array("error" => "No existen productos disponibles.");
		}
	}

	public function obtenerProductosPorCodigo($conn, $codigo){
		$sql  = "SELECT * FROM productos
				WHERE codigo LIKE '%$codigo%'
		";
		$result = $conn->query($sql);
		if(!empty($result)){
			return $result;
		} else {
			return array("error" => "No existen productos para el codigo: ".$codigo);
		}
	}

	public function inter_actualizarProducto($conn, $producto){
		$sql  ="UPDATE productos SET
			tipo 			= '$producto[tipo]',
			calidad 		= '$producto[calidad]',
			expiracion 		= '$producto[expiracion]',
			fechaEditado 	= NOW(),
			activo 			= '$producto[activo]'

			WHERE codigo 	= '$producto[codigo]'
		";
		$result = $conn->query($sql);
		
		if(empty($result)){
			return array("success" => "Producto actulizado correctamente.");
		} else {
			return array("error" => "Error no se pudo actualizar el producto.");
		}
	}
}

?>